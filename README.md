# Blog

Blog sin ningún gestor de contenidos ni generador de sitios estáticos

## Porque
Después de casi 3 años con un blog https://zx81hoy.gitlab.io/ funcionando con Jekyll queria experimental para ver si era posible tener el blog sin tener que usar ningún generador de sitios estáticos.

Aprovechando lo máximo posible la estructura ya creada mediante Jekyll, quiero tener el blog solo usando HTML, CSS y Javascript.
https://salvacam.gitlab.io/blog/

Se mantiene una rama con los últimos cambios usando Jekyll
https://gitlab.com/zx81hoy/zx81hoy.gitlab.io/-/tree/backup_Jekyll?ref_type=heads

## TODO

- Buscador de post por titulo o autor

## Edición del blog

Para comprobar los cambios en local he creado un archivo .sh para levantar un servidor web interno de PHP en el puerto 1234.
En el contenido del archivo **server.sh** se puede modificar el puerto, también esta la opción de comentar la línea 4 y descomentar la línea 5 y que se use python como servidor web.

### Como crear un post nuevo

 1. Crear el archivo html dentro de la carpeta post.

 2. En el archivo js/data.js en el array postList añadir un elemento con los datos del post.

 3. Ejecutar el archivo make_rss.sh para actualizar el archivo public/feed.xml

### Como crear una etiqueta nueva
Solo es necesario añadir la nueva etiqueta en el campo tags del array postList del archivo js/data.js.
Al final del archivo js/data.js se recorre el array postList y se genera un array con todas las etiquetas y el número de post de cada etiqueta.

### Como crear un autor nuevo
Solo es necesario añadir el nombre del autor/a en el campo autor del array postList del archivo js/data.js.
Al final del archivo js/data.js se recorre el array postList y se genera un array con todos las autores y el número de post de cada autora.

### Como crear un año y/o mes nuevo
Solo es necesario añadir la fecha en el campo dateFormat del array postList del archivo js/data.js.
Con el formato dd Mmm AAAA . Ejemplo: "27 Ene 2024"

Al final del archivo js/data.js se recorre el array postList y se genera un array con todos los post por fechas, años y meses.
Tanto las etiquetas, autores y fechas se generan en el mismo foreach del array postList


## Archivos del blog

En la raiz tenemos 4 archivos:
- **make_rss.sh** un script en bash, para actualizar el feed, se debe ejecutar cuando se crea un nuevo post
- **post.sh** un script en bash, para obtener todos los post usados en Jekyll y quitarle el pie, la cabacera y la barra lateral del html y guardarlo con el nombre que uso.
- **README.md** este archivo de documentación
- **server.sh** otro script en bash, sirve para levantar un servidor web y probar lo que se va editando. Es necesario tener PHP o python instalado.

En la carpeta public tenemos los siguientes archivos y carpetas:
- **assets**, carpeta con los recursos (imagenes) que se usan en la web y los post. Los recursos de los post se guardan en una carpeta por año y dentro de ella otra carpeta por mes.
- **css**, carpeta con los archivos CSS 
- **page**, carpeta con los archivos de las páginas estáticas en formato HTML, solo tengo una página (about) si se incluyen más páginas hay que darle la funcionalidad de que carge las nuevas páginas en el archivo **js/main.js**
- **post**, carpeta con con los post en formato HTML.
- **js**, carpeta con los archivos Javascript, contiene dos archivos:
	- **js/data.js** tiene el array **postList** que hay que ir actualizando con cada nuevo post. Recorre ese array y con los datos rellena otros tres arrays (tagList, autorList y dateList) esos arrays se usan en el archivo **js/main.js** para pintar la barra lateral.
	- **js/main.js** este archivo es el centro del blog, cuando se carga un post o una página carga un fichero html de la carpeta post o page y se añade al html
			cuando se hace click en una etiqueta o autor se filtra los post por la etiqueta o autor y se muestran el listado de post, sin la paginación
			la variable postXPage indica el número de post que se mostrara en cada página, no afecta cuando se muestra los post de una etiqueta o autor, 
			y con la variable paginationPage se indica el número de elementos que se mostraran en la paginación, puede tener los valors 1, 3 ó 5
- **index.html**, archivo HTML con la estructura del blog y las plantillas JS que se usan para el listado de post y los elementos de la barra lateral.
- **feed.xml**, archivo XML con la información de los últimos post publicados.

