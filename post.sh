#!/usr/bin/env bash

fileName=""
namePost=""
yearPost=""
monthPost=""
dayPost=""

for y in *; do
 #echo $y

	for m in $y/*; do
 		#echo $m

		for d in $m/*; do
 			#echo $d

			for post in $d/*; do
	 			#echo $post
	 			if [ -f "$post" ]; then
        			#echo "$post"
        			yearPost=$(cut -c 3-4 <<< "$post")
        			#echo "${dayPost}"
        			monthPost=$(cut -c 6-7 <<< "$post")
        			#echo "${monthPost}"
        			dayPost=$(cut -c 9-10 <<< "$post")
        			#echo "${yearPost}"
        			namePost=$(cut -d / -f 4 <<< "$post")
        			#echo "${namePost}"
        			fileName="$yearPost"-"$monthPost"-"$dayPost"-"$namePost"
        			tail -n +1901 "$post" | head -n -35 > "$fileName"
    			fi

			done
		done
	done
done
