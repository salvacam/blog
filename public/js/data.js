
    /***********************************************************/
    /*******                 DATA                     **********/
    /***********************************************************/

  var postList = [
    {
      "href": "24-03-25-Solitaire",
      "asset": "24/03/solitaire.png",
      "title": "Solitaire",
      "date": "2024-03-25",
      "dateFormat": "25 Mar 2024",
      "dateFeed": "Mon, 25 Mar 2024 00:00:00 +0000",
      "tags": ["2024"],
      "autor": "Dr Beep"
    },
    {
      "href": "24-03-24-Tempest",
      "asset": "24/03/tempest.png",
      "title": "Tempest CHR$128",
      "date": "2024-03-24",
      "dateFormat": "24 Mar 2024",
      "dateFeed": "Sun, 24 Mar 2024 00:00:00 +0000",
      "tags": ["2024"],
      "autor": "GCHarder"
    },
    {
      "href": "24-03-19-CityOfAlzan",
      "asset": "24/03/CityOfAlzan.png",
      "title": "City of Alzan",
      "date": "2024-03-19",
      "dateFormat": "19 Mar 2024",
      "dateFeed": "Tue, 19 Mar 2024 00:00:00 +0000",
      "tags": ["2024"],
      "autor": "XavSnap"
    },
    {
      "href": "24-03-18-City-hit",
      "asset": "24/03/Cityhit.png",
      "title": "City Hit",
      "date": "2024-03-18",
      "dateFormat": "18 Mar 2024",
      "dateFeed": "Mon, 18 Mar 2024 00:00:00 +0000",
      "tags": ["2024"],
      "autor": "Richard Szerman"
    },
    {
      "href": "24-03-17-HRLBS",
      "asset": "24/03/HRLBS.png",
      "title": "HRLBS",
      "date": "2024-03-17",
      "dateFormat": "17 Mar 2024",
      "dateFeed": "Tue, 17 mar 2024 00:00:00 +0000",
      "tags": ["2024"],
      "autor": "Dr Beep"
    },
    {
      "href": "24-03-12-RoadkillAlienAtack",
      "asset": "24/03/roadkill.png",
      "title": "Roadkill y Alien Attack",
      "date": "2024-02-12",
      "dateFormat": "12 Mar 2024",
      "dateFeed": "Tue, 12 Mar 2024 00:00:00 +0000",
      "tags": ["2024"],
      "autor": "GCHarder"
    },
    {
      "href": "24-02-27-RetourGenius",
      "asset": "24/02/RetourGenius.png",
      "title": "Le Retour du dr. Genius y Le Mysthère de KiKeKanKoi",
      "date": "2024-02-27",
      "dateFormat": "27 Feb 2024",
      "dateFeed": "Tue, 27 Feb 2024 00:00:00 +0000",
      "tags": ["2024"],
      "autor": "XavSnap"
    },
    {
      "href": "24-02-15-K-Man",
      "asset": "24/02/K-Man.png",
      "title": "K-Man",
      "date": "2024-02-15",
      "dateFormat": "15 Feb 2024",
      "dateFeed": "Thu, 15 Feb 2024 00:00:00 +0000",
      "tags": ["2024"],
      "autor": "Dr Beep"
    },
    {
      "href": "24-02-14-Aquarium",
      "asset": "24/02/Aquarium.png",
      "title": "Aquarium",
      "date": "2024-02-14",
      "dateFormat": "14 Feb 2024",
      "dateFeed": "Wed, 14 Feb 2024 00:00:00 +0000",
      "tags": ["2024", "demo"],
      "autor": "Steven Reid"
    },
    {
      "href": "24-02-13-Swurlpool",
      "asset": "24/02/Swurlpool.png",
      "title": "Swurlpool",
      "date": "2024-02-13",
      "dateFormat": "13 Feb 2024",
      "dateFeed": "Tue, 13 Feb 2024 00:00:00 +0000",
      "tags": ["2024"],
      "autor": "Jonathan Cauldwell"
    },
    {
      "href": "24-02-12-BlackHole",
      "asset": "24/02/BlackHole.png",
      "title": "Black Hole",
      "date": "2024-02-12",
      "dateFormat": "12 Feb 2024",
      "dateFeed": "Mon, 12 Feb 2024 00:00:00 +0000",
      "tags": ["2024"],
      "autor": "GCHarder"
    },
    {
      'href': '24-02-11-3DForestRace',
      'asset': "24/02/3DForestRace.png",
      'title': "3D Forest Race",
      "date": "2024-02-11",
      "dateFormat": "11 Feb 2024",
      'tags': ["2024"],
      'autor': "Briansm"
    },
    {
      'href': '24-02-10-FallingBalls',
      'asset': "24/02/FallingBalls.png",
      'title': "Falling Balls",
      "date": "2024-02-10",
      "dateFormat": "10 Feb 2024",
      'tags': ["2024"],
      'autor': "GCHarder"
    },
    {
      'href': '24-01-11-ZXzine10',
      'asset': "24/01/ZXzine10.png",
      'title': "ZXzine 10",
      "date": "2024-01-11",
      "dateFormat": "11 Ene 2024",
      'tags': ["2024", "revista"],
      'autor': "Tim Swenson"
    },
    {
      'href': '23-12-31-resumen-2023',
      'asset': "23/12/2023.png",
      'title': "Resumen del 2023",
      "date": "2023-12-31",
      "dateFormat": "31 Dic 2023",
      'tags': ["2023", "concurso"],
      'autor': ''
    },
    {
      'href': '23-12-11-Yewdow',
      'asset': "23/12/Yewdow.png",
      'title': "Yewdow",
      "date": "2023-12-11",
      "dateFormat": "11 Dic 2023",
      'tags': ["2023"],
      'autor': "Inufuto"
    },
    {
      'href': '23-12-02-Invader',
      'asset': "23/12/invader.png",
      'title': "Invader",
      "date": "2023-12-02",
      "dateFormat": "02 Dic 2023",
      'tags': ["2023"],
      'autor': "Brian Kumanchik"
    },
    {
      'href': '23-12-01-FunPark',
      'asset': "23/12/funParkZX81.png",
      'title': "Fun Park ZX81",
      "date": "2023-12-01",
      "dateFormat": "01 Dic 2023",
      'tags': ["2023"],
      'autor': "Jonathan Cauldwell"
    },
    {
      'href': '23-11-11-SpinTheSkulls',
      'asset': "23/11/spinTheSkulls.png",
      'title': "Spin the Skulls",
      "date": "2023-11-11",
      "dateFormat": "11 Nov 2023",
      'tags': ["2023"],
      'autor': "GCHarder"
    },
    {
      'href': '23-09-24-Amidoh',
      'asset': "23/09/Amidoh.png",
      'title': "Amidoh!",
      "date": "2023-09-24",
      "dateFormat": "24 Sep 2023",
      'tags': ["2023"],
      'autor': "Jonathan Cauldwell"
    },
    {
      'href': '23-09-20-MadJack',
      'asset': "23/09/MadJack.png",
      'title': "Mad Jack",
      "date": "2023-09-20",
      "dateFormat": "20 Sep 2023",
      'tags': ["2023"],
      'autor': "Johns Bargs"
    },
    {
      'href': '23-09-19-ZXris',
      'asset': "23/09/ZXRIS.png",
      'title': "ZXRIS",
      "date": "2023-09-19",
      "dateFormat": "19 Sep 2023",
      'tags': ["2023"],
      'autor': "Nickmann Studio"
    },
    {
      'href': '23-09-18-k-helmet',
      'asset': "23/09/K-Helmet.png",
      'title': "K-Helmet",
      "date": "2023-09-18",
      "dateFormat": "18 Sep 2023",
      'tags': ["2023"],
      'autor': "Dancresp"
    },
    {
      'href': '23-09-17-Timeblaster',
      'asset': "23/09/Timeblasters.png",
      'title': "Timeblasters",
      "date": "2023-09-17",
      "dateFormat": "17 Sep 2023",
      'tags': ["2023"],
      'autor': "Calliope Software"
    },
    {
      'href': '23-09-16-Arena',
      'asset': "23/09/Arena.png",
      'title': "Arena",
      "date": "2023-09-16",
      "dateFormat": "16 Sep 2023",
      'tags': ["2023"],
      'autor': "GCHarder"
    },
    {
      'href': '23-08-20-3d-Star-Raid',
      'asset': "23/08/3D_Star_Raid.png",
      'title': "3D Star Raid",
      "date": "2023-08-20",
      "dateFormat": "20 Ago 2023",
      'tags': ["2023"],
      'autor': "Bukster"
    },
    {
      'href': '23-08-19-ZX_Nibbler',
      'asset': "23/08/ZX-NIBBLER.png",
      'title': "ZX Nibbler",
      "date": "2023-08-19",
      "dateFormat": "19 Ago 2023",
      'tags': ["2023"],
      'autor': "Johns Bargs"
    },
    {
      'href': '23-08-16-hopman',
      'asset': "23/08/hopman.png",
      "date": "2023-08-16",
      "dateFormat": "16 Ago 2023",
      'title': "Hopman",
      'tags': ["2023"],
      'autor': "Inufuto"
    }
    ,{
      'href': '23-07-30-Cambriolage',
      'asset': "23/07/Cambriolage.png",
      'title': "Cambriolage",
      "date": "2023-07-30",
      "dateFormat": "30 Jul 2023",
      'tags': ["2023"],
      'autor': "Auraes"
    },
    {
      'href': '23-07-29-Breakout',
      'asset': "23/07/breakout.png",
      'title': "ZX81 Breakout",
      "date": "2023-07-29",
      "dateFormat": "29 Jul 2023",
      'tags': ["2023"],
      'autor': "Adrian Pilkington"
    },
    {
      'href': '23-07-18-Man-Khole',
      'asset': "23/07/Man-Khole.png",
      'title': "Man-Khole",
      "date": "2023-07-18",
      "dateFormat": "18 Jul 2023",
      'tags': ["2023"],
      'autor': "Dancresp"
    },
    {
      'href': '23-07-17-superPrograms10',
      'asset': "23/07/superPrograms10.png",
      'title': "Super Programs 10",
      "date": "2023-07-17",
      "dateFormat": "17 Jul 2023",
      'tags': ["2023"],
      'autor': "Dr Beep"
    },
    {
      'href': '23-07-16-MaKarioBros',
      'asset': "23/07/MaKarioBros.png",
      'title': "Ma-Kario Bros",
      "date": "2023-07-16",
      "dateFormat": "16 Jul 2023",
      'tags': ["2023"],
      'autor': "Dancresp"
    },
    {
      'href': '23-07-15-GoldMaze',
      'asset': "23/07/GoldMaze.png",
      'title': "Gold Maze",
      "date": "2023-07-15",
      "dateFormat": "15 Jul 2023",
      'tags': ["2023"],
      'autor': "Dancresp"
    },
    {
      'href': '23-07-11-Terra81',
      'asset': "23/06/terra81.png",
      'title': "Terra 81",
      "date": "2023-07-11",
      "dateFormat": "11 Jul 2023",
      'tags': ["2023"],
      'autor': "Juzzo72"
    },
    {
      'href': '23-07-10-AstroBikers',
      'asset': "23/07/AstroBikers.png",
      'title': "Astro Bikers",
      "date": "2023-07-10",
      "dateFormat": "10 Jul 2023",
      'tags': ["2023"],
      'autor': "Jonathan Cauldwell"
    },
    {
      'href': '23-06-18-lunapark81',
      'asset': "23/06/lunapark81.png",
      'title': "LUNAPARK 81",
      "date": "2023-06-18",
      "dateFormat": "18 Jun 2023",
      'tags': ["2023"],
      'autor': "Dr Beep"
    },
    {
      'href': '23-05-03-81HireGames',
      'asset': "23/05/81HireGames.png",
      'title': "81 hires games for the 1K ZX81",
      "date": "2023-05-03",
      "dateFormat": "03 May 2023",
      'tags': ["2023", "libro"],
      'autor': "Dr Beep"
    },
    {
      'href': '23-05-02-8-bitwars',
      'asset': "23/05/8bitWars.png",
      'title': "8bit wars",
      "date": "2023-05-02",
      "dateFormat": "02 May 2023",
      'tags': ["2023"],
      'autor': "Dr Beep"
    },
    {
      'href': '23-04-26-81tris',
      'asset': "23/04/81-tris.png",
      'title': "81-tris",
      "date": "2023-04-26",
      "dateFormat": "26 Abr 2023",
      'tags': ["2023"],
      'autor': "Salvacam"
    },
    {
      'href': '23-04-15-snake',
      'asset': "23/04/snakeJam.png",
      'title': "Retro Snake Game Jam",
      "date": "2023-04-15",
      "dateFormat": "15 Abr 2023",
      'tags': ["2023", "concurso"],
      'autor': ""
    },
    {
      'href': '23-04-14-qdepthCharge',
      'asset': "23/04/qDepthCharge.png",
      'title': "QDepthCharge",
      "date": "2023-04-14",
      "dateFormat": "14 Abr 2023",
      'tags': ["2023"],
      'autor': "Paul Daniels"
    },
    {
      'href': '23-04-08-LunarLander',
      'asset': "23/04/lunarLander.png",
      'title': "ZX81 Lunar Lander",
      "date": "2023-04-08",
      "dateFormat": "08 Abr 2023",
      'tags': ["2023"],
      'autor': "Adrian Pilkington"
    },
    {
      'href': '23-04-02-10lineas',
      'asset': "23/04/10liners.png",
      'title': "BASIC 10Liner",
      "date": "2023-04-02",
      "dateFormat": "02 Abr 2023",
      'tags': ["2023", "concurso"],
      'autor': ""
    },
    {
      'href': '23-03-27-cracky',
      'asset': "23/03/cracky.png",
      'title': "Cracky",
      "date": "2023-03-27",
      "dateFormat": "27 Mar 2023",
      'tags': ["2023"],
      'autor': "Inufuto"
    },
    {
      'href': '23-03-26-falkensMaze_slicer',
      'asset': "23/03/falkens.png",
      'title': "Falken's Maze y ZX81 Slicer",
      "date": "2023-03-26",
      "dateFormat": "26 Mar 2023",
      'tags': ["2023"],
      'autor': "Adrian Pilkington"
    },
    {
      'href': '23-03-25-yahtk',
      'asset': "23/03/yahtk.png",
      'title': "YAHTK",
      "date": "2023-03-25",
      "dateFormat": "25 Mar 2023",
      'tags': ["2023"],
      'autor': "Dr Beep"
    },
    {
      'href': '23-03-01-CubicMaze',
      'asset': "23/03/Cubicmaze.png",
      'title': "CUBICMAZE",
      "date": "2023-03-01",
      "dateFormat": "01 Mar 2023",
      'tags': ["2023"],
      'autor': "Dr Beep"
    },
    {
      'href': '23-02-15-Tricdel',
      'asset': "23/02/Tricdel.png",
      'title': "Tricdel",
      "date": "2023-02-15",
      "dateFormat": "15 Feb 2023",
      'tags': ["2023"],
      'autor': "Dr Beep"
    },
    {
      'href': '23-02-07-RockCrushII',
      'asset': "23/02/rc2.png",
      'title': "Rock Crush II",
      "date": "2023-02-07",
      "dateFormat": "07 Feb 2023",
      'tags': ["2023"],
      'autor': "Steven McDonald"
    },
    {
      'href': '23-02-06-BouncingDots',
      'asset': "23/02/BounceDot.png",
      'title': "Bouncing Dots",
      "date": "2023-02-06",
      "dateFormat": "06 Feb 2023",
      'tags': ["2023"],
      'autor': "Dr Beep"
    },
    {
      'href': '23-02-05-Qmeteors',
      'asset': "23/02/Qmeteor.png",
      'title': "Qmeteors",
      "date": "2023-02-05",
      "dateFormat": "05 Feb 2023",
      'tags': ["2023"],
      'autor': "Paul Daniels"
    },
    {
      'href': '23-02-04-Wormhole',
      'asset': "23/02/Wormhole.png",
      'title': "Wormhole",
      "date": "2023-02-04",
      "dateFormat": "04 Feb 2023",
      'tags': ["2023"],
      'autor': "Kurt-Arne Johnsen"
    },
    {
      'href': '23-02-03-10= (TENNIS)',
      'asset': "23/02/10=.png",
      'title': "10= (TENNIS)",
      "date": "2023-02-05",
      "dateFormat": "05 Feb 2023",
      'tags': ["2023"],
      'autor': "Dr Beep"
    },
    {
      'href': '23-01-08-maze-sfx',
      'asset': "23/01/mazeDeath.png",
      'title': "Maze Death Race - sound FX edition",
      "date": "2023-01-08",
      "dateFormat": "08 Ene 2023",
      'tags': ["2023"],
      'autor': "Toddy Software"
    },
    {
      'href': '23-01-03-snake',
      'asset': "23/01/snake.png",
      'title': "Snake",
      "date": "2023-01-03",
      "dateFormat": "03 Ene 2023",
      'tags': ["2023"],
      'autor': "Steven Reid"
    },
    {
      'href': '23-01-01-calendario2023',
      'asset': "22/12/calendario2023.png",
      'title': "Calendario 2023",
      "date": "2023-01-03",
      "dateFormat": "03 Ene 2023",
      'tags': ["2023", "calendario"],
      'autor': ""
    },
    {
      'href': '22-12-31-resumen-2022',
      'asset': "22/12/2022.png",
      'title': "Resumen del 2022",
      "date": "2022-12-31",
      "dateFormat": "31 Dic 2022",
      'tags': ["2022", "concurso"],
      'autor': ""
    },
    {
      'href': '22-12-30-zwall',
      'asset': "22/12/wallRun.png",
      'title': "Wall-Run",
      "date": "2022-12-30",
      "dateFormat": "30 Dic 2022",
      'tags': ["2022"],
      'autor': "Orac81"
    },
    {
      'href': '22-12-30-crap2022',
      'asset': "22/12/crap2022.png",
      'title': "Crap Games 2022",
      "date": "2022-12-30",
      "dateFormat": "30 Dic 2022",
      'tags': ["2022", "concurso"],
      'autor': ""
    },
    {
      'href': '22-12-24-even-tree',
      'asset': "22/12/evenTree.png",
      'title': "Even-Tree",
      "date": "2022-12-24",
      "dateFormat": "24 Dic 2022",
      'tags': ["2022"],
      'autor': "Math123"
    },
    {
      'href': '22-12-23-fallzone',
      'asset': "22/12/fallzone.png",
      'title': "Fallzone",
      "date": "2022-12-23",
      "dateFormat": "23 Dic 2022",
      'tags': ["2022"],
      'autor': "Orac81"
    },
    {
      'href': '22-12-20-battlezxone',
      'asset': "22/12/battlezone.png",
      'title': "Battlezxone",
      "date": "2022-12-20",
      "dateFormat": "20 Dic 2022",
      'tags': ["2022"],
      'autor': "Bukster"
    },
    {
      'href': '22-12-16-Krystal',
      'asset': "22/12/Krystal.png",
      'title': "Krystal",
      "date": "2022-12-16",
      "dateFormat": "16 Dic 2022",
      'tags': ["2022", "demo"],
      'autor': "Nickmann Studio"
    },
    {
      'href': '22-12-14-dollarRace-ChaseRace',
      'asset': "22/12/dollarRace.png",
      'title': "Dollar Race y Chase Race",
      "date": "2022-12-14",
      "dateFormat": "14 Dic 2022",
      'tags': ["2022"],
      'autor': "Kurt-Arne Johnsen"
    },
    {
      'href': '22-12-13-ZX81_FlyLowHitHard',
      'asset': "22/12/fly.png",
      'title': "ZX81 Fly Low Hit Hard",
      "date": "2022-12-13",
      "dateFormat": "13 Dic 2022",
      'tags': ["2022"],
      'autor': "Adrian Pilkington"
    },
    {
      'href': '22-12-12-verbix',
      'asset': "22/12/Verbix.png",
      'title': "Verbix",
      "date": "2022-12-13",
      "dateFormat": "13 Dic 2022",
      'tags': ["2022"],
      'autor': "Fabrizio"
    },
    {
      'href': '22-12-11-battle81',
      'asset': "22/12/battle81.png",
      'title': "Battle81",
      "date": "2022-12-11",
      "dateFormat": "11 Dic 2022",
      'tags': ["2022"],
      'autor': "Kurt-Arne Johnsen"
    },
    {
      'href': '22-11-16-Jackpot',
      'asset': "22/11/Jackpot.png",
      'title': "Jackpot",
      "date": "2022-11-16",
      "dateFormat": "16 Nov 2022",
      'tags': ["2022"],
      'autor': "Richard Turnnidge"
    },
    {
      'href': '22-11-07-Guntus',
      'asset': "22/11/Guntus.png",
      'title': "Guntus",
      "date": "2022-11-07",
      "dateFormat": "07 Nov 2022",
      'tags': ["2022"],
      'autor': "Inufuto"
    },
    {
      'href': '22-11-06-RetroDev',
      'asset': "22/11/retroGameDev.png",
      'title': "Retro GAMEDEV 2022",
      "date": "2022-11-06",
      "dateFormat": "06 Nov 2022",
      'tags': ["2022", "concurso"],
      'autor': ''
    },
    {
      'href': '22-10-05-Abduxion',
      'asset': "22/10/Abduxion.png",
      'title': "Abduxion",
      "date": "2022-10-05",
      "dateFormat": "05 Oct 2022",
      'tags': ["2022"],
      'autor': "Jonathan Cauldwell"
    },
    {
      'href': '22-10-04-SuperfiedPipePanic',
      'asset': "22/10/superfiedPipePanic.png",
      'title': "Superfied Pipe Panic",
      "date": "2022-10-04",
      "dateFormat": "04 Oct 2022",
      'tags': ["2022"],
      'autor': "Reboot"
    },
    {
      'href': '22-10-02-DungeonsOfZedd',
      'asset': "22/10/dungeons_of_zedd_beta_2.png",
      'title': "Dungeons of Zedd",
      "date": "2022-10-02",
      "dateFormat": "02 Oct 2022",
      'tags': ["2022"],
      'autor': "Walter Rissi"
    },    
    {
      'href': '22-09-07-Mazy',
      'asset': "22/09/Mazy.png",
      'title': "Mazy",
      "date": "2022-09-07",
      "dateFormat": "07 Sep 2022",
      'tags': ["2022"],
      'autor': "Inufuto"
    },
    {
      'href': '22-09-01-BigBaps',
      'asset': "22/09/BigBaps.png",
      'title': "Big Baps",
      "date": "2022-09-01",
      "dateFormat": "01 Sep 2022",
      'tags': ["2022"],
      'autor': "Jonathan Cauldwell"
    },
    {
      'href': '22-08-28-Ruptus',
      'asset': "22/08/Ruptus.png",
      'title': "Ruptus",
      "date": "2022-08-28",
      "dateFormat": "28 Ago 2022",
      'tags': ["2022"],
      'autor': "Inufuto"
    },
    {
      'href': '22-08-28-Aerial',
      'asset': "22/08/Aerial.png",
      'title': "Aerial",
      "date": "2022-08-28",
      "dateFormat": "28 Ago 2022",
      'tags': ["2022"],
      'autor': "Inufuto"
    },
    {
      'href': '22-08-27-Bootskell',
      'asset': "22/08/Bootskell.png",
      'title': "Bootskell",
      "date": "2022-08-27",
      "dateFormat": "27 Ago 2022",
      'tags': ["2022"],
      'autor': "Inufuto"
    },
    {
      'href': '22-08-27-Battlot',
      'asset': "22/08/Battlot.png",
      'title': "Battlot",
      "date": "2022-08-27",
      "dateFormat": "27 Ago 2022",
      'tags': ["2022"],
      'autor': "Inufuto"
    },
    {
      'href': '22-08-26-neuras',
      'asset': "22/08/neuras.png",
      'title': "Neuras",
      "date": "2022-08-26",
      "dateFormat": "26 Ago 2022",
      'tags': ["2022"],
      'autor': "Inufuto"
    },
    {
      'href': '22-08-25-zimon',
      'asset': "22/08/zimon.png",
      'title': "Zimon",
      "date": "2022-08-25",
      "dateFormat": "25 Ago 2022",
      'tags': ["2022"],
      'autor': "S Anello"
    },
    {
      'href': '22-08-25-memoriez',
      'asset': "22/08/memoriez.png",
      'title': "Ridiculous MemorieZ",
      "date": "2022-08-25",
      "dateFormat": "25 Ago 2022",
      'tags': ["2022"],
      'autor': "S Anello"
    },
    {
      'href': '22-08-24-lift',
      'asset': "22/08/lift.png",
      'title': "Lift",
      "date": "2022-08-24",
      "dateFormat": "24 Ago 2022",
      'tags': ["2022"],
      'autor': "Inufuto"
    },
    {
      'href': '22-08-24-cacorm',
      'asset': "22/08/cacorm.png",
      'title': "Cacorm",
      "date": "2022-08-24",
      "dateFormat": "24 Ago 2022",
      'tags': ["2022"],
      'autor': "Inufuto"
    },
    {
      'href': '22-08-24-ascend',
      'asset': "22/08/ascend.png",
      'title': "Ascend",
      "date": "2022-08-24",
      "dateFormat": "24 Ago 2022",
      'tags': ["2022"],
      'autor': "Inufuto"
    },
    {
      'href': '22-08-08-zxAdventure',
      'asset': "22/08/zxAdventure.png",
      'title': "ZX Adventure",
      "date": "2022-08-08",
      "dateFormat": "08 Ago 2022",
      'tags': ["2022"],
      'autor': "Steven Goodwin"
    },
    {
      'href': '22-08-02-z-zee',
      'asset': "22/08/z-zee.png",
      'title': "Z-ZEE",
      "date": "2022-08-08",
      "dateFormat": "08 Ago 2022",
      'tags': ["2022"],
      'autor': "S Anello"
    },
    {
      'href': '22-08-01-c2Aventura',
      'asset': "22/08/c2Aventura.png",
      'title': "C2 Aventura",
      "date": "2022-08-01",
      "dateFormat": "01 Ago 2022",
      'tags': ["2022"],
      'autor': ''
    },
    {
      'href': '22-07-31-superShogun',
      'asset': "22/07/superShogun.png",
      'title': "Super Shogun",
      "date": "2022-07-31",
      "dateFormat": "31 Jul 2022",
      'tags': ["2022"],
      'autor': "Dr Beep"
    },
    {
      'href': '22-07-15-ZediBlaster',
      'asset': "22/07/zediBlaster.png",
      "date": "2022-07-31",
      "dateFormat": "31 Jul 2022",
      'title': "Zedi Blaster",
      'tags': ["2022"],
      'autor': "Jonathan Cauldwell"
    },
    {
      'href': '22-07-08-Fast',
      'asset': "22/07/Fast.png",
      'title': "Fast Times at Clairmont High",
      "date": "2022-07-08",
      "dateFormat": "08 Jul 2022",
      'tags': ["2022"],
      'autor': "GCHarder"
    },
    {
      'href': '22-07-07-TeeTime',
      'asset': "22/07/TeeTime.png",
      'title': "Tee-Time",
      "date": "2022-07-07",
      "dateFormat": "07 Jul 2022",
      'tags': ["2022"],
      'autor': "XavSnap"
    },
    {
      'href': '22-07-06-Banco',
      'asset': "22/07/Banco.png",
      'title': "Banco",
      "date": "2022-07-07",
      "dateFormat": "07 Jul 2022",
      'tags': ["2022"],
      'autor': ''
    },
    {
      'href': '22-07-05-Hangman',
      'asset': "22/07/Hangman.png",
      'title': "Hangman",
      "date": "2022-07-05",
      "dateFormat": "05 Jul 2022",
      'tags': ["2022"],
      'autor': "Stefano"
    },
    {
      'href': '22-07-04-aritm',
      'asset': "22/07/aritm.png",
      'title': "ARITM",
      "date": "2022-07-04",
      "dateFormat": "04 Jul 2022",
      'tags': ["2022"],
      'autor': "MOB-i-L"
    },
    {
      'href': '22-07-03-ZX81_tetris',
      'asset': "22/07/zx81Tetris.png",
      'title': "ZX81 Tetris",
      "date": "2022-07-03",
      "dateFormat": "03 Jul 2022",
      'tags': ["2022"],
      'autor': "Adrian Pilkington"
    },
    {
      'href': '22-06-12-PaloTGandW',
      'asset': "22/06/paloT_GameAndWatch.png",
      'title': "Palo T Game and Watch",
      "date": "2022-06-12",
      "dateFormat": "12 Jun 2022",
      'tags': ["2022"],
      'autor': "Salvacam"
    },
    {
      'href': '22-06-11-ZX81_snake',
      'asset': "22/06/zx81Snake.png",
      'title': "ZX81 Snake",
      "date": "2022-06-11",
      "dateFormat": "11 Jun 2022",
      'tags': ["2022"],
      'autor': "Adrian Pilkington"
    },
    {
      'href': '22-05-29-valleyOfTheKings',
      'asset': "22/05/valleyOfTheKings.jpg",
      'title': "Valley of the Kings",
      "date": "2022-05-29",
      "dateFormat": "29 May 2022",
      'tags': ["2022"],
      'autor': "Bukster"
    },
    {
      'href': '22-05-14-loderunner',
      'asset': "22/05/loderunner.jpg",
      'title': "Loderunner",
      "date": "2022-05-14",
      "dateFormat": "14 May 2022",
      'tags': ["2022"],
      'autor': "Dr Beep"
    },
    {
      'href': '22-05-06-ur',
      'asset': "22/05/ur.jpg",
      'title': "UR",
      "date": "2022-05-06",
      "dateFormat": "06 May 2022",
      'tags': ["2022"],
      'autor': "Mark Bennett"
    },
    {
      'href': '22-04-30-sbase',
      'asset': "22/04/space-base.jpg",
      'title': "SPACE-BASE",
      "date": "2022-04-30",
      "dateFormat": "30 Abr 2022",
      'tags': ["2022"],
      'autor': "Christoph & Julia"
    },
    {
      'href': '22-04-29-bmx_trial',
      'asset': "22/04/bmxTrial.jpg",
      'title': "BMX Trial",
      "date": "2022-04-29",
      "dateFormat": "29 Abr 2022",
      'tags': ["2022"],
      'autor': "Salvacam"
    },
    {
      'href': '22-04-28-lunares',
      'asset': "22/04/lunares.jpg",
      'title': "Lunares Insaneum",
      "date": "2022-04-28",
      "dateFormat": "28 Abr 2022",
      'tags': ["2022"],
      'autor': "Jonathan Cauldwell"
    },
    { 
      'href': '22-04-26-zeddytron',
      'asset': "22/04/zeddytron.jpg",
      'title': "Zeddytron 2081",
      "date": "2022-04-26",
      "dateFormat": "26 Abr 2022",
      'tags': ["2022"],
      'autor': "Walter Rissi"
    },
    {
      'href': '22-04-25-robbers_of_the_lost_tomb',
      'asset': "22/04/robbers_of_the_lost_tomb.jpg",
      'title': "Robbers of the Lost Tomb",
      "date": "2022-04-25",
      "dateFormat": "25 Abr 2022",
      'tags': ["2022", "traducciones"],
      'autor': "Timeworks"
    },
    {
      'href': '22-04-24-cursedWood',
      'asset': "22/04/cursedWoods.jpg",
      'title': "The Cursed Woods",
      "date": "2022-04-24",
      "dateFormat": "24 Abr 2022",
      'tags': ["2022"],
      'autor': "Christoph & Julia"
    },
    {
      'href': '22-04-24-asteroids_racing',
      'asset': "22/04/asteroids.jpg",
      'title': "ZX81 Asteroids y ZX81 Racing",
      "date": "2022-04-24",
      "dateFormat": "24 Abr 2022",
      'tags': ["2022"],
      'autor': "Adrian Pilkington"
    },
    {
      'href': '22-04-11-potomaniac',
      'asset': "22/04/potomaniac.jpg",
      'title': "Potomaniac",
      "date": "2022-04-11",
      "dateFormat": "11 Abr 2022",
      'tags': ["2022"],
      'autor': "XavSnap"
    },
    {
      'href': '22-04-10-10lineas',
      'asset': "22/04/BASIC_10liner.png",
      'title': "BASIC 10Liner",
      "date": "2022-04-10",
      "dateFormat": "10 Abr 2022",
      'tags': ["2022", "concurso"],
      'autor': ''
    },
    {
      'href': '22-03-12-invincible_island',
      'asset': "22/03/invincible_island.jpg",
      'title': "Invincible Island",
      "date": "2022-03-12",
      "dateFormat": "12 Mar 2022",
      'tags': ["2022"],
      'autor': "Mancave Software"
    },
    {
      'href': '22-03-07-cursed_catacombs',
      'asset': "22/03/cursed_catacombs.jpg",
      'title': "Cursed Catacombs",
      "date": "2022-03-07",
      "dateFormat": "07 Mar 2022",
      'tags': ["2022"],
      'autor': "Walter Rissi"
    },
    {
      'href': '22-03-02-wordle',
      'asset': "22/03/wordle.jpg",
      'title': "Wordle / Lingo",
      "date": "2022-03-02",
      "dateFormat": "02 Mar 2022",
      'tags': ["2022"],
      'autor': "Dr Beep"
    },
    {
      'href': '22-03-01-moskao-fighter',
      'asset': "22/03/moskao-fighter.jpg",
      'title': "Moskao Fighter",
      "date": "2022-03-01",
      "dateFormat": "01 Mar 2022",
      'tags': ["2022"],
      'autor': "Toddy Software"
    },
    {
      'href': '22-01-31-froger-sfx',
      'asset': "22/01/frogger-sfx.jpg",
      'title': "Frogger sfx",
      "date": "2022-01-31",
      "dateFormat": "31 Ene 2022",
      'tags': ["2022"],
      'autor': "Toddy Software"
    },
    {
      'href': '22-01-01-wwiigunman',
      'asset': "22/01/orionsBelt.jpg",
      'title': "WWII Gunman y Orions Belt",
      "date": "2022-01-01",
      "dateFormat": "01 Ene 2022",
      'tags': ["2021", "2022"],
      'autor': "Kurt-Arne Johnsen"
    },
    {
      'href': '21-12-31-resumen-2021',
      'asset': "21/12/2021.jpg",
      'title': "Resumen del 2021",
      "date": "2021-12-31",
      "dateFormat": "31 Dic 2021",
      'tags': ["2021", "concurso"],
      'autor': ''
    },
    {
      'href': '21-12-30-calendario',
      'asset': "21/12/calendario.jpg",
      'title': "Calendario 2022",
      "date": "2021-12-30",
      "dateFormat": "30 Dic 2021",
      'tags': ["2022", "calendario"],
      'autor': ''
    },
    {
      'href': '21-12-29-kingdom',
      'asset': "21/12/kingdom.jpg",
      'title': "Kingdom",
      "date": "2021-12-29",
      "dateFormat": "29 Dic 2021",
      'tags': ["2021", "traducciones"],
      'autor': "Morgan Associates"
    },
    {
      'href': '21-12-28-santa',
      'asset': "21/12/minehunt.jpg",
      'title': "Minehunt y Santa Simulator 1.0",
      "date": "2021-12-28",
      "dateFormat": "28 Dic 2021",
      'tags': ["2021"],
      'autor': "Kurt-Arne Johnsen"
    },
    {
      'href': '21-12-24-cross-snake',
      'asset': "21/12/crosssnake.jpg",
      'title': "Cross Snake",
      "date": "2021-12-24",
      "dateFormat": "24 Dic 2021",
      'tags': ["2021"],
      'autor': "Fabrizio"
    },
    {
      'href': '21-12-23-trader',
      'asset': "21/12/trader.jpg",
      'title': "Trader",
      "date": "2021-12-23",
      "dateFormat": "23 Dic 2021",
      'tags': ["2021", "traducciones"],
      'autor': "Pixel"
    },
    {
      'href': '21-12-22-shrooms',
      'asset': "21/12/shrooms.jpg",
      'title': "Shrooms",
      "date": "2021-12-22",
      "dateFormat": "22 Dic 2021",
      'tags': ["2021"],
      'autor': "Bob's Stuff"
    },
    {
      'href': '21-12-20-crap2021',
      'asset': "21/12/crap2021.jpg",
      'title': "Crap Games 2021",
      "date": "2021-12-20",
      "dateFormat": "20 Dic 2021",
      'tags': ["2021", "concurso"],
      'autor': ''
    },
    {
      'href': '21-12-18-allhires',
      'asset': "21/12/allhires.jpg",
      'title': "All Hires",
      "date": "2021-12-18",
      "dateFormat": "18 Dic 2021",
      'tags': ["2021"],
      'autor': "Dr Beep"
    },
    {
      'href': '21-12-16-omegaphoenix',
      'asset': "21/12/omegaphoenix.jpg",
      'title': "Omega Phoenix",
      "date": "2021-12-16",
      "dateFormat": "16 Dic 2021",
      'tags': ["2021"],
      'autor': "Bukster"
    },
    {
      'href': '21-12-14-qfrogger',
      'asset': "21/12/qfrogger.jpg",
      'title': "QFrogger",
      "date": "2021-12-14",
      "dateFormat": "14 Dic 2021",
      'tags': ["2021"],
      'autor': "Thewiz"
    },
    {
      'href': '21-12-12-spaceinvaders',
      'asset': "21/12/SpaceInvadersforZX81.jpg",
      'title': "Space Invaders for ZX81",
      "date": "2021-12-12",
      "dateFormat": "12 Dic 2021",
      'tags': ["2021"],
      'autor': "SplinterGU"
    },
    {
      'href': '21-11-19-2paxman',
      'asset': "21/11/2paxman.jpg",
      'title': "2PAXMAN",
      'date': '2021-11-19',
      'dateFormat': "19 Nov 2021",
      'tags': ["2021"],
      'autor': "Dr Beep"
    },
    {
      'href': '21-11-07-zedipede',
      'asset': "21/11/zedipede.jpg",
      'title': "Zedipede",
      'date': '2021-11-07',
      'dateFormat': "07 Nov 2021",
      'tags': ["2021"],
      'autor': "Jonathan Cauldwell"
    },
    {
      'href': '21-11-06-ditherpaint81',
      'asset': "21/11/ditherpaint81.jpg",
      'title': "DitherPaint81",
      'date': '2021-11-06',
      'dateFormat': "06 Nov 2021",
      'tags': ["2021"],
      'autor': "Pixelmaker04"
    },
    {
      'href': '21-10-31-retro-snake',
      'asset': "21/10/retro-snake.jpg",
      'title': "Retro Snake",
      'date': '2021-10-31',
      'dateFormat': "31 Oct 2021",
      'tags': ["2021"],
      'autor': "ZX81ultra"
    },
    {
      'href': '21-10-30-sir-clive',
      'asset': "21/10/sir-clive.jpg",
      'title': "Sir Clive",
      'date': '2021-10-30',
      'dateFormat': "30 Oct 2021",
      'tags': ["2021"],
      'autor': "Dr Beep"
    },
    {
      'href': '21-10-29-applepie',
      'asset': "21/10/applepie.jpg",
      'title': "Applepie",
      'date': '2021-10-29',
      'dateFormat': "29 Oct 2021",
      'tags': ["2021"],  
      'autor': "Dr Beep"
    },
    {
      'href': '21-10-27-spider-mutant',
      'asset': "21/10/spider-mutant.jpg",
      'title': "Spider Mutants",
      'date': '2021-10-27',
      'dateFormat': "27 Oct 2021",
      'tags': ["2021"],
      'autor': "Toddy Software"
    }
  ];

  
  var tagList = [];
  var autorList = [];
  var dateList = []; 

  postList.forEach(function(post) {
    if (post.autor != "") {
      if (autorList.filter(autor => autor.autor == post.autor).length > 0) {
        autorList.filter(autor => autor.autor == post.autor)[0].numPost++;
      } else {
        autorList.push({"autor": post.autor, "numPost": 1 });
      }
    }

    post.tags.forEach(function(tagSearch) {
      if (tagList.filter(tag => tag.name == tagSearch).length > 0) {
        tagList.filter(tag => tag.name == tagSearch)[0].numPost++;
      } else {
        tagList.push({"name": tagSearch, "numPost": 1 });
      }
    });

    var year = post.dateFormat.substring(7,11);
    var month = post.dateFormat.substring(3,6);
    var day = post.dateFormat.substring(0,2);
    if (dateList.filter(x => x.year == year).length > 0) {
      if (dateList.filter(x => x.year == year)[0].months.filter(x => x.month == month).length > 0) {
        //the year is exist and the month is exist
        dateList.filter(x => x.year == year)[0].months.filter(x => x.month == month)[0].post.push(
          { "day": day, "title": post.title.substring(0,20), "href": post.href }
        );        
        dateList.filter(x => x.year == year)[0].yearNum++;
      } else {
        //year2023.push({"name": post.month, "numPost": 1 });

        //the year is exist but the month is not exist
        dateList.filter(x => x.year == year)[0].months.push(
          {"month": month, "post": [{ "day": day, "title": post.title.substring(0,20), "href": post.href }]}
        );
        dateList.filter(x => x.year == year)[0].yearNum++;
      }
    } else {
      //the year is not exist
      dateList.push(
        { "year": year,
          "yearNum": 1,
          "months": [
            {"month": month, "post": [{ "day": day, "title": post.title.substring(0,20), "href": post.href }]}
          ]
        });
    }

  });


  autorList.sort((a, b) => {
    const nameA = a.autor.toUpperCase(); // ignore upper and lowercase
    const nameB = b.autor.toUpperCase(); // ignore upper and lowercase
    if (nameA < nameB) {
      return -1;
    }
    if (nameA > nameB) {
      return 1;
    }
    return 0;
  });

  tagList.sort((a, b) => {
    const nameA = a.name.toUpperCase(); // ignore upper and lowercase
    const nameB = b.name.toUpperCase(); // ignore upper and lowercase
    if (nameA < nameB) {
      return -1;
    }
    if (nameA > nameB) {
      return 1;
    }
    return 0;
  });